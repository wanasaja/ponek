<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-04-22 19:08:04
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-04 11:09:36
*/

namespace app\controllers;

use Yii;
use yii\base\view;
use yii\filters\AccessControl;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use app\models\User;
use app\models\Laporan;
use app\models\Rujukan;
use app\models\RujukanSearch;

class LaporanController extends Controller
{   

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => [
                    //admin, user
                    'index', 'export-excel', 'export-pdf'
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'export-excel', 'export-pdf'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_USER,
                        ],
                    ],
                ],
            ],
        ];
    }

	public function actionIndex()
	{	
		$model = new Laporan();

		$searchModel = new RujukanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 0);
		if(Yii::$app->request->isAjax)
		{
			$data = $_POST['Laporan'];
			if($data['dateAwal']==='' && $data['dateAkhir']===''){
		        $dataProvider = $searchModel->searchLaporan(Yii::$app->request->queryParams);
		        $data['dateAwal'] = 'semua';
		        $data['dateAkhir'] = 'semua';
			}
			else{
		        $dataProvider = $searchModel->searchLaporan(Yii::$app->request->queryParams, $data['dateAwal'], $data['dateAkhir']);
			}
			// $dataProvider->pagination->pageSize = 1;
			$dataProvider->pagination = false;
	        return $this->renderPartial('table-laporan-partial', [
	            'searchModel' => $searchModel,
	            'dataProvider' => $dataProvider,

	            'dari' => $data['dateAwal'],
	            'sampai' => $data['dateAkhir']
	        ]);
		}		
		// $dataProvider->pagination->pageSize = 1;
		$dataProvider->pagination = false;
		return $this->render('index',[
			'model' => $model,
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionExportExcel($dari = null, $sampai = null)
    {
        if($dari==='semua' && $sampai==='semua'){
            $namaDokumen = 'Laporan SIR-PONEK (Semua Riwayat Rujukan)';
            if(Yii::$app->user->identity->hak_akses===User::ROLE_ADMIN)
            {
                $dataProvider = Rujukan::find()
                    ->andFilterWhere(['!=', 'status', 'Menunggu'])
                    ->orderBy('tgl_masuk desc')
                    ->all();
            }else
            {
                $dataProvider = Rujukan::find()
                    ->andFilterWhere(['!=', 'status', 'Menunggu'])
                    ->andFilterWhere(['=', 'asal_rujukan', Yii::$app->user->identity->id])
                    ->orderBy('tgl_masuk desc')
                    ->all();
            }            
            $judulDokumen = 'SEMUA RIWAYAT RUJUKAN';
        }else
        {
            $namaDokumen = 'Laporan SIR-PONEK ('.Yii::$app->formatter->asDate($dari).' - '.Yii::$app->formatter->asDate($sampai).')';

            if(Yii::$app->user->identity->hak_akses===User::ROLE_ADMIN)
            {
                $dataProvider = Rujukan::find()
                    ->where(['between', 'tgl_masuk', $dari, $sampai ])
                    ->andFilterWhere(['!=', 'status', 'Menunggu'])
                    ->orderBy('tgl_masuk desc')
                    ->all();
            }else
            {
                $dataProvider = Rujukan::find()
                    ->where(['between', 'tgl_masuk', $dari, $sampai ])
                    ->andFilterWhere(['!=', 'status', 'Menunggu'])
                    ->andFilterWhere(['=', 'asal_rujukan', Yii::$app->user->identity->id])
                    ->orderBy('tgl_masuk desc')
                    ->all();
            }
            $judulDokumen = Yii::$app->formatter->asDate($dari).' - '.Yii::$app->formatter->asDate($sampai);
        }

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        // $template = Yii::getAlias('@web').'/export-excel/data.xlsx';

        $template = Yii::getAlias('@hscstudio/export').'/templates/phpexcel/laporan-petalabumi.xlsx';

        $objPHPExcel = $objReader->load($template);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(\PHPExcel_Worksheet_PageSetup::PAPERSIZE_FOLIO);

        //untuk style border cell
        $styleArray = array(
          'borders' => array(
              'allborders' => array(
                  'style' => \PHPExcel_Style_Border::BORDER_THIN
              )
          )
        );
        $diBorderkan = 6 + count($dataProvider); //2 karena template excel dimulai dari row 3 (baseRow=3)
        $objPHPExcel->getActiveSheet()->getStyle("A5:J".$diBorderkan)->applyFromArray($styleArray);

        $baseRow=7; // line 3
        $no=1;

        //untuk judul laporan didalam EXCEL
        $objPHPExcel->getActiveSheet()->setCellValue('A2', strtoupper(Yii::$app->user->identity->nama_rs_puskesmas));
        $objPHPExcel->getActiveSheet()->setCellValue('A3', strtoupper($judulDokumen));
        //end untuk judul laporan

        foreach($dataProvider as $data){
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$baseRow, $no);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$baseRow, Yii::$app->formatter->asDate($data->tgl_masuk));
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$baseRow, $data->getRelationAsalRujukan());
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$baseRow, $data->getRelationTujuanRujukan());
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$baseRow, $data->nama);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$baseRow, Yii::$app->formatter->asDate($data->tglahir));
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$baseRow, $data->jk);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$baseRow, $data->diagnosa);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$baseRow, $data->status);
            $objPHPExcel->getActiveSheet()->setCellValue('J'.$baseRow, $data->info_balik);
            $baseRow++;$no++;
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$namaDokumen.'.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
        $objWriter->save('php://output');
        exit;
    }

    public function actionExportPdf($dari = null, $sampai = null)
    {
        if($dari==='semua' && $sampai==='semua'){
            $namaDokumen = 'Laporan SIR-PONEK (Semua Riwayat Rujukan)';
            if(Yii::$app->user->identity->hak_akses===User::ROLE_ADMIN)
            {
                $dataProvider = Rujukan::find()
                    ->andFilterWhere(['!=', 'status', 'Menunggu'])
                    ->orderBy('tgl_masuk desc')
                    ->all();
            }else
            {
                $dataProvider = Rujukan::find()
                    ->andFilterWhere(['!=', 'status', 'Menunggu'])
                    ->andFilterWhere(['=', 'asal_rujukan', Yii::$app->user->identity->id])
                    ->orderBy('tgl_masuk desc')
                    ->all();
            }            
            $judulDokumen = 'SEMUA RIWAYAT RUJUKAN';
        }else
        {
            $namaDokumen = 'Laporan SIR-PONEK ('.Yii::$app->formatter->asDate($dari).' - '.Yii::$app->formatter->asDate($sampai).')';

            if(Yii::$app->user->identity->hak_akses===User::ROLE_ADMIN)
            {
                $dataProvider = Rujukan::find()
                    ->where(['between', 'tgl_masuk', $dari, $sampai ])
                    ->andFilterWhere(['!=', 'status', 'Menunggu'])
                    ->orderBy('tgl_masuk desc')
                    ->all();
            }else
            {
                $dataProvider = Rujukan::find()
                    ->where(['between', 'tgl_masuk', $dari, $sampai ])
                    ->andFilterWhere(['!=', 'status', 'Menunggu'])
                    ->andFilterWhere(['=', 'asal_rujukan', Yii::$app->user->identity->id])
                    ->orderBy('tgl_masuk desc')
                    ->all();
            }
            $judulDokumen = Yii::$app->formatter->asDate($dari).' - '.Yii::$app->formatter->asDate($sampai);
        }

        $content = $this->renderPartial('_laporan-pdf',[
            'model'=>$dataProvider,
            'pengguna' => Yii::$app->user->identity->nama_rs_puskesmas,
            'judulDokumen' => $judulDokumen,

        ]);

        date_default_timezone_set('Asia/Jakarta');
        $tanggal = Yii::$app->formatter->asDate(date('d M y'));
        $jam = date('H:i:s');
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_LEGAL, 
            'orientation' => Pdf::ORIENT_LANDSCAPE, 
            // 'destination' => Pdf::DEST_BROWSER, 
            'destination' => Pdf::DEST_DOWNLOAD, 
            'content' => $content,  
            'filename' => $namaDokumen.'.pdf',
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.table > thead > tr > th,.table > tbody > tr > th,.table > tfoot > tr > th,.table > thead > tr > td,.table > tbody > tr > td,.table > tfoot > tr > td { padding: 3px; line-height: 1.42857143; vertical-align: top; border-top: 1px solid #dddddd;}',
            'methods' => [ 
                'SetHeader' => ['Dicetak Otomatis Oleh: SIMPONI||Pada: ' . $tanggal .', '. $jam],
                'SetFooter'=>['Hal. {PAGENO}'],
            ]

        ]);
        
        return $pdf->render(); 
    }

    public function actionCetakTes()
    {
        $namaDokumen = 'Laporan SIR-PONEK (Semua Riwayat Rujukan)';
        $dataProvider = Rujukan::find()
            ->andFilterWhere(['!=', 'status', 'Menunggu'])
            ->orderBy('tgl_masuk desc')
            ->all();
        $judulDokumen = 'SEMUA RIWAYAT RUJUKAN';

        return $this->render('_laporan-pdf',[
            'model'=>$dataProvider,
            'pengguna' => Yii::$app->user->identity->nama_rs_puskesmas,
            'judulDokumen' => strtoupper($judulDokumen),
        ]);
    }
}
