<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-31 13:10:23
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-07 09:35:37
*/


namespace app\controllers;

use Yii;
use yii\base\view;
use yii\filters\AccessControl;
use app\components\AccessRule;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Pengguna;
use app\models\Chat;

use ElephantIO\Client as Socket;
use ElephantIO\Engine\SocketIO\Version1X;

class PesanController extends Controller
{   


    protected $socket;

    public function init()
    {
        $this->socket = new Socket(new Version1X('127.0.0.1:3000'));
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                // We will override the default rule config with the new AccessRule class
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => [
                    //admin, user
                    'index', 'chat', 'kirim'
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'chat', 'kirim'],
                        'allow' => true,
                        'roles' => [
                            User::ROLE_ADMIN,
                            User::ROLE_USER,
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionChat()
    {   
        if(Yii::$app->user->identity->hak_akses==User::ROLE_ADMIN){
            $kontakAwal = Pengguna::find()
                ->select('id, nama_rs_puskesmas')
                ->where(['!=', 'hak_akses', 'super_admin'])
                ->andWhere(['!=', 'hak_akses', 'admin'])
                ->orderBy('nama_rs_puskesmas', SORT_ASC)
                ->one();
            $this->redirect(['pesan/index?id='.$kontakAwal->id]);
        }else
        {
            $this->redirect(['pesan/index?id=2']);
        }
    }



	public function actionIndex()
	{	

        if(Yii::$app->user->identity->hak_akses==User::ROLE_ADMIN){
            $kontak = Pengguna::find()
                ->where(['!=', 'hak_akses', 'super_admin'])
                ->andWhere(['!=', 'hak_akses', 'admin'])
                ->orderBy('nama_rs_puskesmas', SORT_ASC)
                ->all();
        }else
        {
            $kontak = Pengguna::find()
                ->where(['!=', 'hak_akses', 'super_admin'])
                ->andWhere(['!=', 'hak_akses', 'user'])
                ->orderBy('nama_rs_puskesmas', SORT_ASC)
                ->all();
        }

		return $this->render('index',[
			
            'kontak' => $kontak,

		]);
	}

    public function actionKirim()
    {
        date_default_timezone_set('Asia/Jakarta');

        $model = new Chat;
        $model->waktu = date('Y-m-d H:i:s', time());
        $model->dari = $_POST['dari'];
        $model->untuk = $_POST['untuk'];
        $model->isi = $_POST['isi'];
        $model->save();

        
        // if($model->save)
        // {
            $this->socket->initialize();
            $this->socket->emit('chat_kirim', []);
            $this->socket->close();
        // }

        return true;
    }

}
