<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-05 18:08:34
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-05 09:21:41
*/

namespace app\components;

class Penolong {

	public static function label($nilaiLabel)
	{
		$xLabel = [
			'Menunggu' => '<span class="label label-primary" style="font-size: 0.9em;">'.$nilaiLabel.'</span>',
			'Diterima' => '<span class="label label-success" style="font-size: 0.9em;">'.$nilaiLabel.'</span>',
			'Tidak Diterima' => '<span class="label label-warning" style="font-size: 0.9em;">'.$nilaiLabel.'</span>',
		];
		return $xLabel[$nilaiLabel];
	}

	public static function activeChat($a, $b)
	{
		// return 'active';
		if($a==$b) return 'active';
	}

}