/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-06-06 10:18:47
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-06 10:33:13
*/


let objDiv = document.getElementById('kotak-chat');
objDiv.scrollTop = objDiv.scrollHeight;		

$('#chat-kontak').click(function(){
	objDiv.scrollTop = objDiv.scrollHeight;
	console.log('sukses chat-kontak click');
});


//Untuk Kirim Chat
$('#btn-chat').click(function(){
    let isiChat = $('#isi-chat').val();
    if(isiChat){
		kirimChat(isiChat);
    }
}); 

$('#isi-chat').keydown(function (e){
	let isiChat = $('#isi-chat').val();
    if(e.keyCode == 13 && isiChat){
		kirimChat(isiChat);
    }
});	

function kirimChat(isiChat){
		let dari = $('#dari').val();
		let untuk = $('#untuk').val();
    	$.ajax({
		    url: url + '/../../pesan/kirim',
		    method: 'post',
		    data : {
	    		isi : isiChat,
	    		dari : dari,
	    		untuk : untuk
		    },
		    success: function(data) {
				// objDiv.scrollTop = objDiv.scrollHeight;
		        console.log('sukses kirim');
		    }
		});
		$('#isi-chat').val('');

}
//Untuk Kirim Chat End