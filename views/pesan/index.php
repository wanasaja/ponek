<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-31 13:16:31
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-06 14:50:35
*/

use yii\helpers\Url;
use yii\widgets\Pjax;
use app\components\Penolong;

use app\models\Chat;
use app\models\ChatSearch;

use app\assets\ChatAsset;
ChatAsset::register($this);

$this->title = 'Pesan';

?>

<div class="row">
	
	<div class="col-md-12">
		
		

		<!-- view kontak -->
		<div class="col-md-4" style="background-color: white">

			<div class="nav-chat list-group">
				<?php
					// $aktif = 'active';
					foreach($kontak as $k=>$key)
					{
						// if($k!=0){
						// 	$aktif = '';
						// }
						echo '
							<a id="chat-kontak" href="?id='.$kontak[$k]->id.'" class="list-group-item '.Penolong::activeChat($kontak[$k]->id, $_GET['id']).'">
								<h5 class="list-group-item-heading"> <li class="fa fa-user"></li> <strong><em>'.$kontak[$k]->nama_rs_puskesmas.'</em></strong></h5>
							  </a>
						';
						
					}
					
				?>
			</div>

		</div>
	
		<!-- view chat -->
		<div class="col-md-8" style="background-color: white">
			
			<!-- <?= $_GET['id'] ?> -->

			<!-- <br><br> -->

			<div class="container">
			<div class="row">
		        <div class="col-md-8">
		            <div class="panel panel-primary">
		                <div class="panel-heading">
		                    <span class="glyphicon glyphicon-comment"></span> Chat
		                </div>

		                

		                <div class="panel-body" id="kotak-chat">
		                	<?php Pjax::begin([
								'id' => 'pjax-chat',
							]); ?>
		                    <ul class="chat">
		                    	<!-- untuk ambil chat -->
								<?php 
									$chat = Chat::kotakChat(Yii::$app->user->identity->id, $_GET['id']);

									foreach ($chat as $c)
									{
										if($c->dari==Yii::$app->user->identity->id)
										{
											echo ' <li class="right clearfix"><span class="chat-img pull-right">
						                            <img src="'.Yii::getAlias('@web').'/img/user.jpg" alt="User Avatar" class="img-circle" width="45px"/>
						                        </span>
					                            <div class="chat-body clearfix">
					                                <div class="header">
					                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>'.$c->waktu.'</small>
					                                    <strong class="pull-right primary-font">'.$c->dari_text.'</strong>
					                                </div>
					                                <p>
					                                    '.$c->isi.'
					                                </p>
					                            </div>
					                        </li>';
										}else
										{
											echo '
												<li class="left clearfix"><span class="chat-img pull-left">
						                            <img src="'.Yii::getAlias('@web').'/img/user-you.jpg" alt="User Avatar" class="img-circle" width="45px"/>
						                        </span>
						                            <div class="chat-body clearfix">
						                                <div class="header">
						                                    <strong class="primary-font">'.$c->dari_text.'</strong> <small class="pull-right text-muted">
						                                        <span class="glyphicon glyphicon-time"></span>'.$c->waktu.'</small>
						                                </div>
						                                <p>
						                                    '.$c->isi.'
						                                </p>
						                            </div>
						                        </li>
											';
										}
										
									}

								?>

		                        
		                        <!-- <li class="right clearfix"><span class="chat-img pull-right">
		                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
		                        </span>
		                            <div class="chat-body clearfix">
		                                <div class="header">
		                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
		                                    <strong class="pull-right primary-font">Bhaumik Patel</strong>
		                                </div>
		                                <p>
		                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
		                                    dolor, quis ullamcorper ligula sodales.
		                                </p>
		                            </div>
		                        </li> -->

		                    </ul>
		                    <?php Pjax::end(); ?>
		                </div>
						

						

		                <div class="panel-footer">
		                    <div class="input-group">
		                    	<input type="hidden" value="<?= Yii::$app->user->identity->id ?>" id="dari">
		                    	<input type="hidden" value="<?= $_GET['id'] ?>" id="untuk">
		                        <input id="isi-chat" type="text" class="form-control input-sm" placeholder="Ketik pesan anda disini..." />
		                        <span class="input-group-btn">
		                            <button class="btn btn-warning btn-sm" id="btn-chat">
		                                Send</button>
		                        </span>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

		</div>


	</div>

</div>

<?php

$this->registerJs($this->render('index.js'));