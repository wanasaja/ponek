<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-04-22 16:44:37
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-04 10:13:49
*/

use yii\helpers\Html;
use yii\helpers\Url;
// use yii\bootstrap\ActiveForm;
use kartik\form\ActiveForm;
use kartik\icons\Icon;

use app\assets\ModalAsset;


ModalAsset::register($this);

$this->title = 'Masuk';
?>
<style>
    .tooltip-inner {
        min-width: 100px;
        max-width: 100%; 
    }

    .modal-dialog {
        top: 7%;
    }

    div#modalHeader h4 {
        color: #ffffff;
    }
</style>

<div class="" style="text-align: center">
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        
        <div style="">
        <img class="center-block" style="margin: 0 auto; padding-right:0px" width="120px" src="<?=Yii::getAlias('@web')?>/logo/logo_ponek.png"/>
        </div>
    
    </div>
    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        
        <div style="">
        
        <span style="font-weight:bold; padding-left:15px; font-family: 'Poppins', sans-serif; font-size:38px;"><strong>S I M P O N I</strong></span>

        </div>
    
    </div>
</div>

<div id="" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" style="padding-bottom: 50px;"> 
    
    <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Masuk</div>
                <!-- <div style="float:right; font-size: 80%; position: relative; top:-10px"><a data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pilih ini jika anda lupa Sandi" href="#">Lupa Sandi?</a></div> -->
                <div style="float:right; font-size: 100%; position: relative; top:-23px">
                <?=Html::button(
                    'Lupa Password?',
                    [
                        // 'url' => '',info
                        'title' => 'Lupa Password?',
                        'value' => Url::to(['tamu/lupa-pass']),
                        'header' => 'Ubah Password',
                        'class' => 'modalButton btn-info btn-xs',
                        'style' => 'width:100%',
                        // 'style' => 'float:right; font-size: 0%; position: relative; top:-10px'
                    ]
                )?>
                </div>
            </div>     

            <div style="padding-top:30px" class="panel-body" >

                <!-- <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div> -->
                
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    // 'type' => ActiveForm::TYPE_HORIZONTAL,
                ]); ?>

                        <?= $form->field($model, 'username', [
                            'addon' => [
                                'prepend' => ['content'=>'<i class="fa fa-user"></i>']
                            ],
                        ])->label(false)->textInput([
                            'placeholder'=>'Masukkan Username Anda',
                            'autofocus' => true
                        ]) ?>     
                            
                        <?= $form->field($model, 'password', [
                            'addon' => [
                                'prepend' => ['content'=>'<i class="fa fa-lock"></i>']
                            ],
                        ])->label(false)->passwordInput([
                            'placeholder'=>'Masukkan Password Anda',
                        ]) ?>  

                        <div class="form-group">
                            <div class ="">
                                <?= Html::submitButton(Icon::show('sign-in').' &nbsp;Masuk Sekarang', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>
                        </div>

                  <?php ActiveForm::end(); ?>


                </div>                     
            </div>  
</div>


<?php  //js code: 
    $this->registerJs(
    "$(document).on('ready pjax:success', function() {
            $('.modalButton').click(function(e){
            e.preventDefault(); //for prevent default behavior of <a> tag.
            $('#modal').modal('show');  
        });
        });
    ")
?>

<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => [
        'id' => 'modalHeader',
        'style' => 'background-color: #085886;'
    ],
    'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>',
    // 'header' => 'Ubah Profil',
    'id' => 'modal',
    'size' => 'modal-lg',
    // 'clientOptions' => ['backdrop' => 'static', 'keyboard' => true]
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
?>
