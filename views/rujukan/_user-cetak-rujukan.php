<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-12 12:24:22
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-06-04 10:22:56
*/

use yii\helpers\Url;
use yii\widgets\DetailView;
use barcode\barcode\BarcodeGenerator as BarcodeGenerator;

$pesanQr = $model->tgl_masuk.'\n'.$model->nama.'\n'.strtoupper($model->status);

?>

<p>
	<table style="width:100%">
		<tbody>
			<tr>
				<td style="" width="20%">
					<barcode code="<?= $pesanQr ?>" disableborder="1" type="QR" class="barcode" size="0.8" error="M"/>
				</td>
				<td style="text-align:center;" width="60%"><h4 style="padding-top:8px;margin-bottom:15px;margin-left:15%"><strong>Sistem Informasi Rujukan<br>PONEK Terintegrasi</strong></h4></td>
				<td style="text-align:right;" width="20%">
					<img class="center-block" style="margin: 0 auto; padding-bottom:3px; padding-right:0px" width="90px" src="<?=Yii::getAlias('@web')?>/logo/logo_ponek.png"/>
				</td>
			</tr>
		</tbody>
	</table>
</p>
	

<div class="panel panel-default" style="">
	<div class="panel-body">

	<h4 style="margin-bottom:15px;text-align: center;"><u>Data Umum Pasien</u></h4>

	<table id="w0" class="table detail-view">
		<tbody>
			<tr>
				<th width="40%" style="padding:3px;">Tanggal Masuk</th><td style="padding:3px;"><?= Yii::$app->formatter->asDate($model->tgl_masuk) ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Jenis Pasien</th><td style="padding:3px;"><?= $model->jenis ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Nama</th><td style="padding:3px;"><?= $model->nama ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Jenis Kelamin</th><td style="padding:3px;"><?= $model->jk ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Tempat Lahir</th><td style="padding:3px;"><?= $model->tplahir ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Tanggal Lahir</th><td style="padding:3px;"><?= $model->tglahir ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Umur</th><td style="padding:3px;"><?= $model->umur ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Alamat</th><td style="padding:3px;"><?= $model->alamat ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">Cara Bayar</th><td style="padding:3px;"><?= $model->cara_bayar ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">NIK</th><td style="padding:3px;"><?= $model->nik ?></td>
			</tr>
			<tr>
				<th width="40%" style="padding:3px;">No BPJS/JKD</th><td style="padding:3px;"><?= $model->no_bpjs_jkd ?></td>
			</tr>
		</tbody>
	</table>  
	</div>
</div>



<div class="panel panel-default">
	<div class="panel-body">

		<h4 style="margin-bottom:15px;text-align: center;"><u>Rujukan &amp; Resume Pasien</u></h4>

		<table id="w1" class="table detail-view">
			<tbody>
				<tr>
					<th width="40%" style="padding:3px;">Asal Rujukan</th><td style="padding:3px;"><?= $model->asal_rujukan_text ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Tujuan Rujukan</th><td style="padding:3px;"><?= $model->tujuan_rujukan_text ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Alasan Rujukan</th><td style="padding:3px;"><?= $model->alasan_rujukan ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Anamnesa</th><td style="padding:3px;"><?= $model->anamnesa ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Kesadaran</th><td style="padding:3px;"><?= $model->kesadaran ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Tekanan Darah</th><td style="padding:3px;"><div class="row"><span class="col-md-6" style="margin-right:25%;"><?= $model->tekanan_darah ?></span> <i>mmHg</i></div></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Nadi</th><td style="padding:3px;"><div class="row"><span class="col-md-1" style="margin-right:25%;"><?= $model->nadi ?></span> <i>x/menit</i></div></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Suhu</th><td style="padding:3px;"><div class="row"><span class="col-md-1" style="margin-right:25%;"><?= $model->suhu ?></span> <i>C</i></div></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Pernapasan</th><td style="padding:3px;"><div class="row"><span class="col-md-1" style="margin-right:25%;"><?= $model->pernapasan ?></span> <i>x/menit</i></div></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Nyeri</th><td style="padding:3px;"><?= $model->nyeri ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Pemeriksaan Fisik</th><td style="padding:3px;"><?= $model->pemeriksaan_fisik ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Pemeriksaan Penunjang</th><td style="padding:3px;"><?= $model->pemeriksaan_penunjang ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Diagnosa</th><td style="padding:3px;"><?= $model->diagnosa ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Tindakan Yang Sudah Diberikan</th><td style="padding:3px;"><?= $model->tindakan_yg_sdh_diberikan ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Status</th><td style="padding:3px;"><?= strtoupper($model->status) ?></td>
				</tr>
				<tr>
					<th width="40%" style="padding:3px;">Info Balik</th><td style="padding:3px;"><?= $model->info_balik ?></td>
				</tr>
			</tbody>
		</table>  
	</div>
</div>

	<div style="text-align: center; width:25%;">
	<strong>PETUGAS</strong>
	<br><br><br><br>
	________________________
	</div>