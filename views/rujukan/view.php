<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-11 17:37:35
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-11 17:39:20
*/

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rujukan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rujukans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rujukan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'tgl_masuk',
            'jenis',
            'nama',
            'jk',
            'tplahir',
            'tglahir',
            'umur',
            'alamat:ntext',
            'cara_bayar',
            'nik',
            'no_bpjs_jkd',
            'asal_rujukan',
            'tujuan_rujukan',
            'alasan_rujukan:ntext',
            'anamnesa:ntext',
            'kesadaran',
            'tekanan_darah',
            'nadi',
            'suhu',
            'pernapasan',
            'nyeri',
            'pemeriksaan_fisik:ntext',
            'pemeriksaan_penunjang:ntext',
            'diagnosa:ntext',
            'tindakan_yg_sdh_diberikan:ntext',
            'info_balik:ntext',
            'status',
        ],
    ]) ?>

</div>
