<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-11 17:37:35
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-25 10:38:35
*/

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use kartik\dialog\Dialog;

/* @var $this yii\web\View */
/* @var $model app\models\Rujukan */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .modal-dialog {
        top: 7%;
    }
</style>

<div class="rujukan-form">

    <?php $form = ActiveForm::begin([
            // 'id' => 'login-form-horizontal', 
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
        ]);
    ?>

    <div class="row">
        <div class="col-md-6">
        <div class="panel panel-primary">
              <div class="panel-heading">Data Umum Pasien</div>
              <div class="panel-body">
                
                <?php
                    $jenisPasien = [
                        'Ibu' => 'Ibu',
                        'Bayi' => 'Bayi'
                    ];
                    echo $form->field($model, 'jenis')->radioButtonGroup($jenisPasien, [
                        'class' => 'btn-group-md',
                        'itemOptions' => [
                            'labelOptions' => ['class' => 'jenisPasien btn btn-default'],
                            'id'=>'rujukan-jenis-item'
                        ],
                    ]);
                ?>
                
                <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>
                
                <?php
                    // $jk = [
                    //     'Laki-laki' => 'Laki-laki',
                    //     'Perempuan' => 'Perempuan'
                    // ];
                    // echo $form->field($model, 'jk',  ['options' => ['class' => 'form-group', 'id'=>'rujukan-jenis-group']])->radioButtonGroup($jk, [
                    //     'class' => 'btn-group-md',
                    //     'itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']],
                    //     'id' => 'fg-jk'
                    // ]);
                ?>
                <div id="rujukan-jenis-group" class="form-group field-fg-jk required">
                    <?php
                        $model->jk = 'Perempuan';
                        echo $form->field($model, 'jk')->hiddenInput()->label(false);
                    ?>
                </div>

                <?= $form->field($model, 'tplahir')->textInput(['maxlength' => true]) ?>

                <?php
                    echo $form->field($model, 'tglahir')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Pilih Tanggal Lahir', 'id'=>'date2'],
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                            'endDate' => '+0d',
                            'todayHighlight' => true,
                            // 'todayBtn' => true,
                        ]
                    ]);
                ?>

                <?= $form->field($model, 'umur')->textInput(['maxlength' => true, 'readOnly'=> true]) ?>

                <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

                <?php
                    $caraBayar = [
                        'UMUM' => 'UMUM',
                        'BPJS' => 'BPJS',
                        'JKD' => 'JKD'
                    ];
                    echo $form->field($model, 'cara_bayar')->widget(Select2::classname(), [
                        'data' => $caraBayar,
                        'options' => [
                            'placeholder' => 'Pilih Cara Pembayaran...',
                        ],
                    ]);      
                ?>
                
                <div id="update-form-cara-bayar">
                    <?= Html::activeHiddenInput($model, 'nik', ['value' => $model->nik]);  ?>
                    <?= Html::activeHiddenInput($model, 'no_bpjs_jkd', ['value' => $model->no_bpjs_jkd]);  ?>

                    <?php // $form->field($model, 'nik')->textInput(['maxlength' => true]) ?>

                    <?php // $form->field($model, 'no_bpjs_jkd')->textInput() ?>
                    
                </div>

              </div>
          </div>
      </div>
        <div class="col-md-6">
        <div class="panel panel-primary">
              <div class="panel-heading">Rujukan & Resume Pasien</div>
              <div class="panel-body">

                <?= Html::activeHiddenInput($model, 'asal_rujukan', ['value' => $model->asal_rujukan]);  ?>
                <?= Html::activeHiddenInput($model, 'tujuan_rujukan', ['value' => $model->tujuan_rujukan]);  ?>

                <?php // $form->field($model, 'asal_rujukan')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <?php // $form->field($model, 'tujuan_rujukan')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <div class="form-group field-rujukan-asal_rujukan has-success">
                    <label class="control-label col-sm-3" for="rujukan-asal_rujukan">Asal Rujukan</label>
                    <div class="col-sm-9">
                        <?= Html::textInput('',$model->asal_r, ['class' => 'form-control', 'readOnly' => true]) ?>
                        <div class="help-block"></div>
                    </div>
                </div>

                <div class="form-group field-rujukan-tujuan_rujukan has-success">
                    <label class="control-label col-sm-3" for="rujukan-tujuan_rujukan">Tujuan Rujukan</label>
                    <div class="col-sm-9">
                        <?= Html::textInput('',$model->tujuan_r, ['class' => 'form-control', 'readOnly' => true]) ?>
                        <div class="help-block"></div>
                    </div>
                </div>

                <?= $form->field($model, 'alasan_rujukan')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'anamnesa')->textarea(['rows' => 6]) ?>
                
                <?php
                    $kesadaran = [
                        'Sadar' => 'Sadar',
                        'Tidak Sadar' => 'Tidak Sadar'
                    ];
                    echo $form->field($model, 'kesadaran')->radioButtonGroup($kesadaran, [
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']],
                    ]);
                ?>

                <div class="form-group field-rujukan-tekanan_darah">
                    <label class="control-label col-sm-3" for="rujukan-tekanan_darah">Tekanan Darah</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'tekanan_darah',[
                                    'addon' => [ 
                                        'append' => ['content' => 'mmHg', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput([])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <div class="form-group field-rujukan-nadi">
                    <label class="control-label col-sm-3" for="rujukan-nadi">Nadi</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'nadi',[
                                    'addon' => [ 
                                        'append' => ['content' => 'x/menit', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput([])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <div class="form-group field-rujukan-suhu">
                    <label class="control-label col-sm-3" for="rujukan-suhu">Suhu</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'suhu',[
                                    'addon' => [ 
                                        'append' => ['content' => 'C', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput([])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <div class="form-group field-rujukan-pernapasan">
                    <label class="control-label col-sm-3" for="rujukan-pernapasan">Pernapasan</label>
                    <div class="col-sm-9">
                        <div style="width:100%;">
                            <?= $form->field($model, 'pernapasan',[
                                    'addon' => [ 
                                        'append' => ['content' => 'x/menit', 'options'=>['class'=>'alert-primary']],
                                    ]
                                ])->textInput([])->label(false) 
                            ?>
                        </div>
                    </div>
                </div>

                <?php
                    $nyeriOpsi = [
                        'Nyeri Berat' => 'Nyeri Berat',
                        'Nyeri Sedang' => 'Nyeri Sedang',
                        'Nyeri Ringan' => 'Nyeri Ringan'
                    ];
                    echo $form->field($model, 'nyeri')->radioButtonGroup($nyeriOpsi, [
                        'class' => 'btn-group-md',
                        'itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']],
                    ]);
                ?>


                <?= $form->field($model, 'pemeriksaan_fisik')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'pemeriksaan_penunjang')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'diagnosa')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'tindakan_yg_sdh_diberikan')->textarea(['rows' => 6]) ?>

                <?php echo $form->field($model, 'info_balik')->hiddenInput(['rows' => 6])->label(false) ?>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <?php 
                        echo Dialog::widget([
                            'overrideYiiConfirm' => true,
                            'options' => [
                                'size' => Dialog::SIZE_LARGE, // large dialog text
                                'type' => Dialog::TYPE_PRIMARY,
                                'title' => '<i class="fa fa-exclamation-circle"></i> Konfirmasi',
                                'btnOKClass' => 'btn-primary',
                                'btnOKLabel' => '<i class="fa fa-check"></i> Ya, Benar.',
                                'btnCancelLabel' => '<i class="fa fa-ban"></i> Kembali'
                            ],

                        ]);
                        echo Html::submitButton('Simpan', [
                            'class' => 'btn btn-primary',
                            "data" => [
                                'confirm' => 'Apakah Data Sudah Benar?',
                            ],    
                        ]) ?>
                    </div>
                </div>

              </div>
          </div>
      </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs("
    $(document).ready(function(){

        const url = window.location;

        $('.btn-group .jenisPasien').on('click', function(){
            console.log($(this).text())
            const jk = $(this).text().trim();
            $.ajax({
                type: \"POST\",
                url: url+\"/../../rujukan/update-jk\",
                data: {id:jk},
                success: function(result){
                    console.log('ajax sukses');
                    $('#rujukan-jenis-group').replaceWith(result);
                }
            });
        });


        $('#date2').on('change', function(){
            let dateNya = $('#date2').val();
            console.log(dateNya);
            $.ajax({
                type: \"POST\",
                url: url+\"/../../rujukan/update-umur\",
                data: {id:dateNya},
                success: function(result){
                    console.log('ajax sukses');
                    console.log(result);
                    $('#rujukan-umur').val(result);
                }
            });
        });

        $('#rujukan-cara_bayar').on('change', function(){
            let caraBayarNya = $('#rujukan-cara_bayar').val();
            console.log(caraBayarNya);
            $.ajax({
                type: \"POST\",
                url: url+\"/../../rujukan/update-cara-bayar\",
                data: {id:caraBayarNya},
                success: function(result){
                    console.log('ajax sukses');
                    console.log(result);
                    $('#update-form-cara-bayar').replaceWith(result);
                }
            });
        });

    });
");

?>